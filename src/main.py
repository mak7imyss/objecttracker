import queue
import threading
import time

import cv2

from utils.Logger import Logger
from ObjectDetector import ObjectDetector
from EdoLogic import EdoLogic

logger = Logger('main')

# Function to clear the queue
def clearQueue(dataQueue):
    while not dataQueue.empty():
        dataQueue.get()

# Function for the child thread to process coordinates
def childProcess(stopEvent, dataQueue):
    while not stopEvent.is_set():
        try:
            coord = dataQueue.get(timeout=1)
        except queue.Empty:
            continue
        time.sleep(1)
        # Check if coordinates are within the desired range and conditions
        if coord is not None and (0 <= coord['dXmm'] <= 5) and (0 <= coord['dYmm'] <= 5) and coord['isEmpty'] is False and coord['isMoving'] is False:
            logger.debug(coord)
            edo.setStartState()
            edo.toBooty(coord, colorCurrent)
            clearQueue(dataQueue)
            time.sleep(1)
        clearQueue(dataQueue)
    logger.info("Child thread is exiting...")

if __name__ == "__main__":
    # Open the video capture device
    cap = cv2.VideoCapture(2)

    # Create an instance of EdoLogic and connect to it
    edo = EdoLogic('192.168.1.232')
    edo.connect()
    edo.setStartState()

    # Create an instance of ObjectDetector
    detector = ObjectDetector()

    # Define the video writer for saving the video without HUD
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640, 480))

    # Define the video writer for saving the video with HUD
    outWithHud = cv2.VideoWriter('outputWuthHud.avi', fourcc, 20.0, (443, 190))

    # Create an Event object to signal the child thread to stop
    stopEvent = threading.Event()

    # Create a queue to communicate coordinates between threads
    dataQueue = queue.Queue()
    coord = None
    dataQueue.put(coord)

    # Create the child thread
    childThread = threading.Thread(target=childProcess, args=(stopEvent, dataQueue,))

    # Start the child thread
    childThread.start()

    colorSearchRetries = 0
    colors = ['yellow', 'red', 'green', 'cyan']
    colorCurrent = 0

    while True:
        # Read a frame from the video capture device
        ret, frame = cap.read()

        # Write the frame to the output video file
        out.write(frame)

        # Crop the frame to the desired region of interest
        frame = frame[95:285, 76:519]

        # If color search retries exceed the limit, switch to the next color
        if colorSearchRetries > 10:
            colorSearchRetries = 0
            colorCurrent = (colorCurrent + 1) % 4
            clearQueue(dataQueue)

        # Detect objects and obtain coordinates for the current color
        coord = detector.detect(frame, colors[colorCurrent])

        # Write the frame with HUD to the output video file
        outWithHud.write(frame)

        # If the detected region is empty, increment color search retries
        if coord['isEmpty']:
            colorSearchRetries += 1

        # Put the coordinates in the queue for the child thread
        dataQueue.put(coord)

        # Display the frame
        cv2.imshow("Frame", frame)

        # Check for the 'q' key press to stop the program
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
            stopEvent.set()
            break

    # Release resources
    cap.release()
    out.release()
    outWithHud.release()
    cv2.destroyAllWindows()