import time

from pyedo import eduedo

from utils.Logger import Logger

logger = Logger('Edo')
class EdoLogic:
    # List of places for throwing off objects
    places = [150, 50, -50, -150]
    def __init__(self, LAN_IP = '192.168.12.1'):
        self.edo = eduedo(LAN_IP)

    def connect(self):
        # Initialize 7 axes
        self.edo.init7Axes()
        # Unblock Edo
        self.edo.unblock()
        time.sleep(1)
        logger.info('Connect')

    def setStartState(self):
        # Move joints to the start state
        self.edo.moveJoints(0.00, 0.00, 80.0, 0.00, 98.7, -45.00)
        time.sleep(1)
        logger.info('Start state set')

    def toBooty(self, coord, place = 0):
        x, y, a = coord['cXmm'], coord['cYmm'], coord['angle']
        time.sleep(1)
        # Move to the specified Cartesian coordinates
        self.edo.moveCartesian(x, y, 80, 45 + a, 180, 0)
        time.sleep(1)
        # Open gripper
        self.edo.moveGripper(60)
        time.sleep(1)
        # Move down to grab the object
        self.edo.moveCartesian(x, y, 20, 45 + a, 180, 0)
        time.sleep(1)
        # Close gripper
        self.edo.moveGripper(0)
        # Move back up
        self.edo.moveJoints(0.00, 0.00, 80.0, 0.00, 98.7, -45.00)
        time.sleep(1)
        logger.info('Booty completed')
        self.throwOff(place)

    def throwOff(self, place):
        # Move up
        self.edo.moveJoints(0.00, 90.00, 45.0, 0.00, -45.00, -45.00)
        time.sleep(1)
        # Move down to the specified throwing off place
        self.edo.moveCartesian(695, self.places[place], 150, 0, 90, -45)
        time.sleep(1)
        # Open gripper to throw off the object
        self.edo.moveGripper(60)
        time.sleep(1)
        logger.info('Reset completed')
        self.setStartState()
