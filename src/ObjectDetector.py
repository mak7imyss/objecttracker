import math

import cv2
import numpy as np

from utils.Logger import Logger

logger = Logger('Detector')

class ObjectDetector:
    # Define colors in HSV color space
    colors = {
        'yellow': ((17, 158, 205), (35, 255, 255)),
        'red': ((0, 158, 0), (16, 255, 255)),
        'green': ((35, 158, 190), (40, 255, 255)),
        'cyan': ((66, 158, 110), (108, 255, 255))
    }


def __init__(self):
    # Initialize previous centroid coordinates and angle
    self.prev_cX = None
    self.prev_cY = None
    self.prev_angle = None
    self.isEmpty = False
    self.isMoving = False
    self.cXmm = 0
    self.cYmm = 0
    self.prev_cXmm = -1
    self.prev_cYmm = -1


def detect(self, frame, color):
    # Convert BGR image to HSV color space
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # Apply color mask to extract specified color
    mask = cv2.inRange(hsv, *self.colors[color])

    # Find contours of objects in the mask
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    if len(contours) > 0:
        self.isEmpty = False
        self.isMoving = False
        # Find contour with maximum area
        c = max(contours, key=cv2.contourArea)

        # Calculate centroid of contour
        M = cv2.moments(c)
        if M["m00"] != 0:
            self.prev_cXmm = self.cXmm
            self.prev_cYmm = self.cYmm
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
            self.cXmm = 317 + int(134 / 190 * cY)
            self.cYmm = 169 - int(325 / 443 * cX)
        else:
            cX, cY = 0, 0

        # Check object motion
        if self.prev_cX is not None and self.prev_cY is not None:
            rect = cv2.minAreaRect(c)
            box = cv2.boxPoints(rect)
            box = np.intp(box)
            center = (int(rect[0][0]), int(rect[0][1]))

            edge1 = np.intp((box[1][0] - box[0][0], box[1][1] - box[0][1]))
            edge2 = np.intp((box[2][0] - box[1][0], box[2][1] - box[1][1]))

            usedEdge = edge1
            if cv2.norm(edge2) > cv2.norm(edge1):
                usedEdge = edge2

            reference = (1, 0)
            if cv2.norm(reference) == 0 or cv2.norm(usedEdge) == 0:
                angle = 0
            else:
                angle = round(180.0 / math.pi * math.acos(
                    (reference[0] * usedEdge[0] + reference[1] * usedEdge[1]) /
                    (cv2.norm(reference) * cv2.norm(usedEdge))))

            deltaX = abs(cX - self.prev_cX)
            deltaY = abs(cY - self.prev_cY)
            if angle is not None and self.prev_angle is not None:
                deltaAngle = abs(round(angle, 2) - round(self.prev_angle, 2))
            else:
                deltaAngle = 0

            if deltaX <= 20 and deltaY <= 20:
                self.displayed_X = cX
                self.displayed_Y = cY
                # Output centroid coordinates and angle of rotation
                if self.prev_angle is None or deltaAngle > 5:
                    logger.debug(f"Centroid coordinates: ({cX}, {cY}), Angle of rotation: {angle} degrees")
                    self.prev_angle = angle
            else:
                self.isMoving = True
                logger.info("Object is moving")

                # Draw centroid, contours, and position on the frame
                cv2.drawContours(frame, [box], 0, (255, 0, 0), 2)
                cv2.circle(frame, center, 5, (0, 0, 128), 2)
                cv2.putText(frame, f'Axes x: {cX}', (15, 25),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 0, 0), 2, cv2.LINE_AA)
                cv2.putText(frame, f'Axes y: {cY}', (15, 50),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 0, 0), 2, cv2.LINE_AA)
                cv2.putText(frame, f'Angle: {angle}', (15, 75),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 128), 2, cv2.LINE_AA)
        else:
            angle = None
        self.prev_cX = cX
        self.prev_cY = cY
        self.prev_angle = angle
    else:
        if not self.isEmpty:
            logger.info("Object not found")
            self.isEmpty = True

    return self.getPosition()

def getPosition(self):
    # Calculate the change in X and Y coordinates
    if self.prev_cXmm is not None:
        dXmm = abs(self.prev_cXmm - self.cXmm)
    else:
        dXmm = -1
    if self.prev_cYmm is not None:
        dYmm = abs(self.prev_cYmm - self.cYmm)
    else:
        dYmm = -1

    return {
        'cXmm': self.cXmm,
        'cYmm': self.cYmm,
        'angle': self.prev_angle,
        'cX': self.prev_cX,
        'cY': self.prev_cY,
        'dXmm': dXmm,
        'dYmm': dYmm,
        'isEmpty': self.isEmpty,
        'isMoving': self.isMoving
    }