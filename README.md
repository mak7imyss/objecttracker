# Robotic Arm Object Detection

This project utilizes the e.DO robotic arm and object detection to perform specific tasks based on the detected objects. The system is designed to detect objects of different colors and sort them using the e.DO robotic arm.

## Requirements

- Python >= 3.9
- OpenCV >= 4.7.0
- PyEdo >= 0.7

## Installation

1. Clone the repository:

```shell
git clone https://gitlab.com/mak7imyss/objecttracker.git
```

2. Install the required dependencies:

```shell
pip install -r package.txt
```

3. Connect the robotic arm to the computer and make sure it is properly configured.

## Usage

1. Run the main.py script:

```shell
python src/main.py
```

2. The program will open a video feed from the camera connected to the computer.

3. The robotic arm will perform object detection on the captured frames.

4. When a suitable object is detected, the robotic arm will grab it and throw it off to a specified location.

5. The process continues until the program is terminated.

## Customization

- You can modify the colors and their corresponding HSV ranges in the ObjectDetector.py file to detect different colored objects.

- Adjust the coordinates and movements in the EdoLogic.py file to suit your specific robotic arm and desired actions.

- Modify the parameters and conditions in the main.py file to customize the behavior of the system.

## Contributing

Contributions to this project are welcome. Please open an issue or submit a pull request with any improvements or bug fixes.

## Contact

For any questions or inquiries, please contact [@mak7imyss](https://t.me/mak7imyss).
