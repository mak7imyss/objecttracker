import logging
import sys

# Создаем класс логгера
class Logger:
    def __init__(self, service, logLevel=logging.INFO):
        self.logger = logging.getLogger(service)
        self.logger.setLevel(logLevel)

        # Создаем обработчик, который будет выводить сообщения в консоль
        console_handler = logging.StreamHandler(sys.stdout)
        console_handler.setLevel(logLevel)

        # Создаем форматтер для определения структуры сообщений лога
        formatter = logging.Formatter('%(asctime)s %(levelname)s: [%(name)s] %(message)s')

        # Привязываем форматтер к обработчику
        console_handler.setFormatter(formatter)

        # Добавляем обработчик к логгеру
        self.logger.addHandler(console_handler)

        # Настраиваем цвета уровней логирования
        self._setup_colors()

    def _setup_colors(self):
        # Создаем цветовые коды для уровней логирования
        ERROR_COLOR = '\x1b[31m'  # Красный
        WARNING_COLOR = '\x1b[33m'  # Желтый
        DEBUG_COLOR = '\x1b[90m'  # Темно серый
        RESET_COLOR = '\x1b[0m'  # Сброс цвета

        # Настраиваем цвета для каждого уровня логирования
        logging.addLevelName(logging.ERROR, f'{ERROR_COLOR}{logging.getLevelName(logging.ERROR)}{RESET_COLOR}')
        logging.addLevelName(logging.WARNING, f'{WARNING_COLOR}{logging.getLevelName(logging.WARNING)}{RESET_COLOR}')
        logging.addLevelName(logging.DEBUG, f'{DEBUG_COLOR}{logging.getLevelName(logging.DEBUG)}{RESET_COLOR}')

    def debug(self, message):
        self.logger.debug(message)

    def info(self, message):
        self.logger.info(message)

    def warn(self, message):
        self.logger.warning(message)

    def error(self, message):
        self.logger.error(message)
